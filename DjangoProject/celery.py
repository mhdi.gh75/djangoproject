# DjangoProject/celery.py

import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DjangoProject.settings")
app = Celery("DjangoProject")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


app.conf.beat_schedule = {
    'encrypt_expired_products-every-day': {
        'task': 'core.tasks.check_waiting_payment_status',
        'schedule': 1200
    }
}

