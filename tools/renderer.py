from rest_framework.renderers import JSONRenderer


class CustomRenderer(JSONRenderer):

    def render(self, data, accepted_media_type=None, renderer_context=None):
        status_code = renderer_context['response'].status_code
        response = {
          "status": "success",
          "code": status_code,
          "data": data,
          "message": None
        }

        if str(status_code) == '404':
            response["status"] = 404
            response["message"] = 'صفحه مورد نظر یافت نشد'
            try:
                response["data"] = data["detail"]
            except KeyError:
                response["data"] = data

        if str(status_code) == '401':
            response["status"] = 401
            response["message"] = 'سطح دسترسی مجاز نیست'
            try:
                response["data"] = data["detail"]
            except KeyError:
                response["data"] = data
        
        if str(status_code) == '400':
            response["status"] = 400
            response["message"] = 'مقادیر مجاز نیست'
            try:
                response["data"] = data["detail"]
            except KeyError:
                response["data"] = data
                
        if str(status_code) == '500':
            response["status"] = 500
            response["message"] = 'خطا در سرور'
            try:
                response["data"] = data["detail"]
            except KeyError:
                response["data"] = data
                
        if str(status_code).startswith('2'):
            response["status"] = status_code
            response["message"] = 'عملیات با موفقیت انجام شد'
            try:
                response["data"] = data
            except KeyError:
                response["data"] = data
             
        return super(CustomRenderer, self).render(response, accepted_media_type, renderer_context)
