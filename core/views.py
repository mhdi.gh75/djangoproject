from django.utils import timezone
import json
import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from .serializers import RegisterSerializer, UserProfileSerializer, AuhtenticationSerializer, TicketSerializer \
                        , AccountSerializer, UserRequestSerializer,  WithdrawalRequestSerializer, ChallengeSerializer \
                        , UserChallengesSerializer, UpdatePasswordSerializer, ForgetPasswordSerializer
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.core import mail
from django.db import transaction
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.models import Token
from .models import User, UserAuthentication, Ticket, Account, UserRequest, WithdrawalRequest, Challenges, UserChallenges
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from django.http.response import Http404
from tools.renderer import CustomRenderer
from .actions import generate_verification_code, sendmail, generate_new_password, payment, generate_new_orderid, post_payment
from config import *
import logging
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from DjangoProject import settings


db_logger = logging.getLogger('db')

    
class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer
    renderer_classes = [CustomRenderer]
    
class GetProfileView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request, email):
        user = User.objects.filter(email=email).first()
        token = AccessToken.for_user(user)
        refresh = RefreshToken.for_user(user)
        return Response(
            data={
                'access_token': str(token),
                'refresh_token': str(refresh)
            }
        )


class ProfileView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = [CustomRenderer]
    def get(self, request):
        user = request.user

        final_response =  {
                'id': user.id,
                'email': user.email,
                'fullname': user.fullname,
                'first_name_en': user.first_name_en,
                'last_name_en': user.last_name_en,
                'phone_number': user.phone_number,
                'is_authorized': user.is_authorized,
                'date_joined': user.date_joined,
                'is_email_verified': user.is_email_verified
            }
        return Response(data=final_response, status=200)

    def put(self, request, *args, **kwargs):
        user = self.request.user
        serializer = UpdatePasswordSerializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            is_valid = user.check_password(serializer.validated_data['password'])
            if not is_valid:
                return Response({"detail": "پسورد نادرست است."}, status=400)
            
            print(serializer.validated_data)
            user.set_password(serializer.validated_data['new_password'])
            user.save()
            return Response({'detail': 'پسورد با موفقیت تغییر یافت'}, status=200)
        else:
            return Response({"detail": "پسورد نادرست است."}, status=400)
        

class VerificationView(APIView):
    permission_classes = [AllowAny, ]
    renderer_classes = [CustomRenderer]
    
    def get(self, request, secret_code):
        user = User.objects.filter(verification_secret=secret_code).first()
        if user:
            user.is_email_verified = True
            user.verification_secret = None
            user.save()
            
            return Response(data={}, status=200)
        
        else:
            raise Http404


class RequestVerificationView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = [CustomRenderer]
    
    def post(self, request, *args):
        user = self.request.user
        if user.is_email_verified:
            return Response({'detail': 'اکانت شما فعال است'}, status=400)

        secret_code = generate_verification_code()
        verification_link = "{0}/users/verify/{1}".format(settings.BASE_URL, secret_code)
        sendmail(
            "تایید ایمیل",            
            "برای تایید ایمیل روی لینک زیر کلیک کنید \n {}".format(verification_link),
            [user.email],
        )
        
        return Response(data={}, status=200)
      
class ForgetPasswordView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = [CustomRenderer]
    
    def post(self, request, *args):
        serializer = ForgetPasswordSerializer(data=request.data)
        if serializer.is_valid():
            password = generate_new_password()
            sendmail(
                "فراموشی رمز عبور",            
                "پسورد جدید ثبت شده برای اکانت شما {0} میباشد".format(password),
                [serializer.validated_data['email']],
            )
            user = User.objects.filter(email=serializer.validated_data['email']).first()
            user.set_password(password)
            user.save()
            return Response(data={}, status=200)

        else:
            return Response({'detail': 'درخواست مورد نظر مجاز نیست'}, status=400)
            
           
# class UpdateProfileView(APIView):
#     permission_classes = (IsAuthenticated,)
#     renderer_classes = [CustomRenderer]
#     def put(self, request, *args, **kwargs):
#         user = self.request.user
#         serializer = UserProfileSerializer(user, data=request.data, partial=True)
#         if serializer.is_valid():
#             serializer.save()
        
#         else:
#             return Response({serializer.errors}, status=400)
#         return Response(data={'User Updated'}, status=200)


class AuthenticationView(viewsets.ModelViewSet):
    queryset = UserAuthentication.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = AuhtenticationSerializer
    renderer_classes = [CustomRenderer]
    
    def list(self, request, *args, **kwargs):
        user = self.request.user
        authentication_object = UserAuthentication.objects.filter(user=user).first()
        if not authentication_object:
            return Response(data={}, status=200)
        response = {
                'id': authentication_object.id,
                'user': authentication_object.user.id,
                'national_number': authentication_object.national_number,
                'birth_date': str(authentication_object.birth_date),
                'national_card_image': str(authentication_object.national_card_image),
                'user_with_card_image': str(authentication_object.user_with_card_image),
                'is_authorized': authentication_object.is_authorized
            }
        
        return Response(data=response)
    
    def create(self, request, *args, **kwargs):
        user = self.request.user
        request.data._mutable = True
        request.data['user'] = user.id
        
        return super().create(request, *args, **kwargs)


    def update(self, request, *args, **kwargs):
        user = self.request.user
        request.data['user'] = user.id
        obj = self.get_object()
        if obj.is_authorized:
            return Response({'detail': 'این اکانت تایید شده است.'}, status=400)
        return super().update(request, args, **kwargs)
    
    
class TicketView(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = TicketSerializer
    renderer_classes = [CustomRenderer]
    
    def create(self, request, *args, **kwargs):
        user = self.request.user
        request.data['creator'] = user.id
        return super().create(request, *args, **kwargs)
    
    def list(self, request, *args, **kwargs):
        user = self.request.user
        ticket_object = Ticket.objects.filter(creator=user).order_by('-created_ts')
        final_response = []
        for obj in ticket_object:
            
            response = {
                'id': obj.id,
                'creator': obj.creator.id,
                'title': obj.title,
                'description': obj.description,
                'email': obj.email,
                'created_ts': obj.created_ts
            }
            final_response.append(response)
            
        return Response(data=final_response)


class AccountView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = AccountSerializer
    renderer_classes = [CustomRenderer]

    def get_queryset(self):
        accounts = Account.objects.filter(user=self.request.user).order_by('-created_ts')
        return accounts
    
    def create(self, request, *args, **kwargs):
        user = self.request.user
        request.data['user'] = user.id
        return super().create(request, *args, **kwargs)
    
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
    
    
class UserRequestView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserRequestSerializer
    renderer_classes = [CustomRenderer]
    
    def get_queryset(self):
        requests = UserRequest.objects.filter(creator=self.request.user).order_by('-created_ts')
        return requests

    def create(self, request, *args, **kwargs):
        user = self.request.user
        request.data['creator'] = user.id
        return super().create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
    
    
    
class WithdrawalRequestView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = WithdrawalRequestSerializer
    renderer_classes = [CustomRenderer]
    
    def get_queryset(self):
        requests = WithdrawalRequest.objects.filter(creator=self.request.user).order_by('-created_ts')
        return requests

    def create(self, request, *args, **kwargs):
        user = self.request.user
        request.data['creator'] = user.id
        return super().create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
    

class ChallengeView(viewsets.ModelViewSet):
    queryset = Challenges.objects.all().order_by('-created_ts')
    permission_classes = (IsAuthenticated,)
    serializer_class = ChallengeSerializer
    renderer_classes = [CustomRenderer]
    
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)


class BuyChallengeView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserChallengesSerializer
    renderer_classes = [CustomRenderer]
    
    def post(self, request, *args):
        user = self.request.user
        challenge_id = request.data['challenge']
        try:
            challenge = Challenges.objects.get(id=challenge_id)
        except Exception as e:
            raise Http404
        
        amount = challenge.price
        # Pay Amount
        order_id = generate_new_orderid()
        payment_data = payment(amount, request.data['price_currency'],request.data['pay_currency'], order_id)
        post_payment_data = post_payment(payment_data['id'], payment_data['pay_currency'])
        
        
        UserChallenges.objects.create(
            challenges=challenge,
            user=user,
            created_ts=timezone.now(),
            purchase_amount=payment_data['price_amount'],
            purchase_status='waiting',
            status='waiting',
            purchase_ts=timezone.now(),
            order_id=payment_data['order_id'],
            invoice_id=payment_data['id'],
            invoice_url=payment_data['invoice_url'],
            price_currency=payment_data['price_currency'],
            pay_currency=payment_data['pay_currency'],
            purchase_id=post_payment_data['payment_id'],
            pay_address=post_payment_data['pay_address'],
            pay_amount=post_payment_data['pay_amount']
        )
        
        return Response(data=payment_data, status=200)

class GetUserChallenges(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = [CustomRenderer]
    
    def get(self, request):
        user = self.request.user
        challenges = UserChallenges.objects.filter(user=user).order_by('-created_ts')
        final_response = []
        for obj in challenges:
            final_response.append(
                {
                    'id': obj.id,
                    'challenge': obj.challenges.id,
                    'challenge_name': obj.challenges.name,
                    'status': obj.status,
                    'created_ts': obj.created_ts,
                    'purchase_id': obj.purchase_id,
                    'purchase_ts': obj.purchase_ts,
                    'amount': obj.purchase_amount
                }
            )

        return Response(data=final_response)
    

class GetCurrencies(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = [CustomRenderer]
    
    def get(self, request):
        url = NOWPAYMENT_BASE_URL + 'merchant/coins'
        headers = {
            'x-api-key': NOWPAYMENT_TOKEN
        }
        res = requests.get(url, headers=headers)
        if res.status_code == 200:
            coins = res.json()['selectedCurrencies']
            return Response(data=coins)
        else:
            return Response({'detail': 'ارتباط برقرار نشد'}, status=400)
            
        
class GetMinimumValidAmountView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = [CustomRenderer]
    
    def get(self, request, _from, _to):
        url = NOWPAYMENT_BASE_URL + 'min-amount?currency_from={0}&currency_to={1}'.format(_from, _to)
        headers = {
            'x-api-key': NOWPAYMENT_TOKEN
        }
        res = requests.get(url, headers=headers)
        if res.status_code == 200:
            amount = res.json()['min_amount']
            return Response(data={
                    'min_amount': amount,
                    'from': _from,
                    'to': _to
                })
        else:
            return Response({'detail': 'ارتباط برقرار نشد'}, status=400)
            