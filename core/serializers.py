from rest_framework import serializers
from .models import User, UserAuthentication, Ticket, Account, UserRequest, WithdrawalRequest, Challenges, UserChallenges
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from django.core.mail import send_mail
from DjangoProject import settings
import random
import string
from .actions import sendmail, generate_verification_code



class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    fullname = serializers.CharField(write_only=True, required=True)
    
    class Meta:
        model = User
        fields = ('password', 'password2', 'email', 'fullname')
        extra_kwargs = {
            'email': {'required': True},
            'password': {'required': True},
            'password2': {'required': True},
            'fullname': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            fullname=validated_data['fullname']
        )

        
        user.set_password(validated_data['password'])
        secret_code = generate_verification_code()
        
        user.verification_secret = secret_code
        user.save()

        verification_link = "{0}/users/verify/{1}".format(settings.BASE_URL, secret_code)
        
        sendmail(
            "تایید ایمیل",            
            "برای تایید ایمیل روی لینک زیر کلیک کنید \n {}".format(verification_link),
            [validated_data['email']],
        )
        
        return user


class UpdatePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    new_password = serializers.CharField(write_only=True, required=True)
    class Meta:
        model = User
        fields = ('password', 'new_password')
        extra_kwargs = {
            'new_password': {'required': True},
            'password': {'required': True},
        }
        

class ForgetPasswordSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    class Meta:
        model = User
        fields = ('email',)
        extra_kwargs = {
            'email': {'required': True}
        }
    
    def validate(self, attrs):
        qs = User.objects.filter(email=attrs['email']).first()
        if not qs:
            raise serializers.ValidationError({"email": "ایمیل وارد شده مجاز نیست"})
        return attrs



class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'fullname', 'first_name_en', 'last_name_en', 'email', 'phone_number', 'is_email_verified')
        

class AuhtenticationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAuthentication
        fields = ('id', 'user', 'national_number', 'birth_date', 'national_card_image', 'user_with_card_image')
        

class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = ('id', 'creator', 'title', 'email', 'description', 'created_ts')
    
    
class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'user', 'account_address', 'balance', 'created_ts')
        

class UserRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserRequest
        fields = ('id', 'creator', 'account', 'title', 'description', 'created_ts', 'status')
        

class WithdrawalRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = WithdrawalRequest
        fields = ('id', 'creator', 'amount')


class ChallengeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Challenges
        fields = '__all__'


class UserChallengesSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserChallenges
        fields = '__all__'