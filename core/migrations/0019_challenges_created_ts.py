# Generated by Django 4.2 on 2023-05-29 12:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_ticket_image_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='challenges',
            name='created_ts',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
