# Generated by Django 4.2 on 2023-04-23 16:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_ticket_is_solved'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userrequest',
            options={'verbose_name_plural': 'UserRequest'},
        ),
        migrations.AddField(
            model_name='userrequest',
            name='status',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.CreateModel(
            name='WithdrawalRequest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.CharField(max_length=15)),
                ('created_ts', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(max_length=255)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'WithdrawalRequest',
            },
        ),
    ]
