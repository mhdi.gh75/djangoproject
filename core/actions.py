from django.core.mail import send_mail
from DjangoProject import settings
import random
import requests
import string
from .models import User
from config import *
import json

def sendmail(title, content, recipients):
    send_mail(
        title,
        content,
        settings.EMAIL_HOST_USER,
        recipients,
        fail_silently=False)


def generate_verification_code():
    letters = string.ascii_lowercase
    secret_code = ''.join(random.choice(letters) for i in range(12))
    user = User.objects.filter(verification_secret=secret_code).first()
    while user:
        secret_code = ''.join(random.choice(letters) for i in range(12))
        user = User.objects.filter(verification_secret=secret_code).first()
    
    return secret_code


def generate_new_password():
    letters = string.ascii_lowercase
    password = ''.join(random.choice(letters) for i in range(12))
    return password

def generate_new_orderid():
    letters = string.ascii_lowercase
    order = ''.join(random.choice(letters) for i in range(4)).upper()
    id = random.randrange(10000,99999)
    orderid = "{0}_{1}".format(order, str(id))
    return orderid

def payment(price_amount, price_currency, pay_currency, order_id):
    url = NOWPAYMENT_BASE_URL + 'invoice'
    
    headers = {
        'x-api-key': NOWPAYMENT_TOKEN
    }
    payload = {
        "price_amount": price_amount,
        "price_currency": price_currency,
        "pay_currency": pay_currency,
        "order_id": order_id,
        "ipn_callback_url": IPN_CALLBACK_URL,
        "success_url": IPN_SUCCESS_URL,
        "cancel_url": IPN_CANCEL_URL
    }
    res = requests.post(url, data=payload, headers=headers)
    return res.json()


def check_payment_status(payment_id):
    url = NOWPAYMENT_BASE_URL + 'payment/{0}'.format(payment_id)
    
    headers = {
        'x-api-key': NOWPAYMENT_TOKEN
    }
    res = requests.get(url, headers=headers)
    return res.json()


def post_payment(invoice_id, pay_currency):
    url = NOWPAYMENT_BASE_URL + 'invoice-payment'
    
    headers = {
        'x-api-key': NOWPAYMENT_TOKEN
    }
    payload = {
        "iid": invoice_id,
        "pay_currency": pay_currency
    }
    
    res = requests.post(url, data=payload, headers=headers)
    return res.json()