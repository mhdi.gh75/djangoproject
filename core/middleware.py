import logging
import json

db_logger = logging.getLogger('db')

class UserActivityLogMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if request.path == '/auth/login/':
            if 'access' in json.loads(response.content):
                db_logger.debug("User Token is {0}".format(json.loads(response.content)['access']))
            else:
                db_logger.debug("Login Attempt Failed " )
        return response