# feedback/tasks.py

from time import sleep
from celery import shared_task
from .models import UserChallenges
from .actions import check_payment_status
from django.db.models import Q
from celery import shared_task, Celery

app = Celery('DjangoProject',
             broker='redis://localhost:6379/0',
             backend='redis://localhost:6379/0',
             )


@app.task
def check_waiting_payment_status():
    qs = UserChallenges.object.filter(~Q(purchase_status='finished'))
    for obj in qs:
        data = check_payment_status(obj.purchase_id)
        obj.purchase_status = data['payment_status']
        obj.status = data['payment_status']
        obj.actually_paid = data['actually_paid']
        obj.pay_address = data['pay_address']
        obj.save()
        