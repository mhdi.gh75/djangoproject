from django.contrib import admin

# Register your models here.
# -*- encoding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.admin.models import LogEntry
from django.contrib import admin
from auditlog.admin import LogEntryAdmin
from .models import User, UserAuthentication, Ticket, UserRequest, WithdrawalRequest, Challenges, UserChallenges, Account


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ['fullname', 'email', 'first_name_en', 'last_name_en', 'phone_number', 'is_authorized', 'date_joined']
    def save_model(self, request, obj, form, change):
        form.data._mutable = True
        obj.set_password(form.data['password'])
        form.data.pop('password')
        super().save_model(request, obj, form, change)

@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    model = Ticket
    list_display = ['creator', 'title', 'email', 'description', 'created_ts']
    

@admin.register(UserAuthentication)
class AuthorizationAdmin(admin.ModelAdmin):
    model = UserAuthentication
    list_display = ['user', 'national_number', 'birth_date', 'national_card_image', 'user_with_card_image', 'is_authorized', 'created_ts']



@admin.register(UserRequest)
class UserRequestAdmin(admin.ModelAdmin):
    model = UserRequest
    list_display =  [field.name for field in UserRequest._meta.get_fields()]



@admin.register(WithdrawalRequest)
class WithdrawalRequestAdmin(admin.ModelAdmin):
    model = WithdrawalRequest
    list_display =  [field.name for field in WithdrawalRequest._meta.get_fields()]


@admin.register(Challenges)
class ChallengeAdmin(admin.ModelAdmin):
    model = Challenges
    list_display =  [field.name for field in Challenges._meta.get_fields() if field.name != 'userchallenges']


@admin.register(UserChallenges)
class UserChallengesAdmin(admin.ModelAdmin):
    model = UserChallenges
    list_display =  [field.name for field in UserChallenges._meta.get_fields()]



@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    model = Account
    list_display =  [field.name for field in Account._meta.get_fields() if field.name != 'userrequest']



