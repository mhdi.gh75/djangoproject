# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path
from django.contrib.auth.views import LogoutView
from .views import RegisterView, ProfileView, AuthenticationView, TicketView, AccountView \
                , UserRequestView, WithdrawalRequestView, ChallengeView, BuyChallengeView \
                , GetUserChallenges, VerificationView, RequestVerificationView \
                , ForgetPasswordView, GetCurrencies, GetMinimumValidAmountView, GetProfileView
urlpatterns = [
    path('register/', RegisterView.as_view(), name='auth_register'),
    # path('users/update/', UpdateProfileView.as_view(), name='update_profile'),
    path('users/list/', ProfileView.as_view(), name='get_profile'),
    path('auth/callback/<str:email>/', GetProfileView.as_view(), name='google_account_api'),
    path('users/password/update/', ProfileView.as_view(), name='update_password'),
    path('users/forget-password/', ForgetPasswordView.as_view(), name='forget_password'),
    path('users/verify/', RequestVerificationView.as_view(), name='request_verification'),
    path('users/verify/<str:secret_code>/', VerificationView.as_view(), name='verify_user'),
    path('users/authentications/create/', AuthenticationView.as_view({'post': 'create'}), name='authenticate_user'),
    path('users/authentications/list/', AuthenticationView.as_view({'get': 'list'}), name='get_authenticate'),
    path('users/authentications/update/<int:pk>/', AuthenticationView.as_view({'put': 'update'}), name='update_authenticate'),
    path('users/tickets/create/', TicketView.as_view({'post':'create'}), name='create_token'),
    path('users/tickets/list/', TicketView.as_view({'get':'list'}), name='create_token'),
    path('users/accounts/create/', AccountView.as_view({'post': 'create'}), name='create_account'),
    path('users/accounts/list/', AccountView.as_view({'get': 'list'}), name='get_account'),
    path('users/requests/create/', UserRequestView.as_view({'post': 'create'}), name='create_request'),
    path('users/requests/list/', UserRequestView.as_view({'get': 'list'}), name='get_request'),
    path('users/withdraws/create/', WithdrawalRequestView.as_view({'post': 'create'}), name='create_withdraw'),
    path('users/withdraws/list/', WithdrawalRequestView.as_view({'get': 'list'}), name='get_withdraw'),
    path('challenges/list/', ChallengeView.as_view({'get': 'list'}), name='get_challenges'),
    path('challenges/buy/', BuyChallengeView.as_view(), name='buy_challenge'),    
    path('users/challenges/list/', GetUserChallenges.as_view(), name='get_user_challenges'),
    path('users/payment/currencies/', GetCurrencies.as_view(), name='get_valid_currencies'),
    path('users/payment/min-amount/<str:_from>/<str:_to>/', GetMinimumValidAmountView.as_view(), name='get_minimum_amount')
]