# -*- encoding: utf-8 -*-
from auditlog.registry import auditlog
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from datetime import timedelta


from django.contrib.auth.models import BaseUserManager
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

class CustomUserManager(BaseUserManager):
    """
    Custom user manager where the username field is replaced with the email field.
    """

    def create_user(self, email, password=None, **extra_fields):
        """
        Create and save a user with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email field must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        """
        Create and save a superuser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))

        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    username = None
    
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    
    email = models.EmailField(blank=False, null=False, unique=True)
    password = models.CharField(max_length=100, blank=False, null=False)
    fullname = models.CharField(max_length=150, blank=False)
    first_name_en = models.CharField(max_length=30, blank=True)
    last_name_en = models.CharField(max_length=30, blank=True)
    phone_number = models.CharField(max_length=12, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_authorized = models.BooleanField(default=False)
    is_email_verified = models.BooleanField(default=False)
    verification_secret = models.CharField(max_length=12, blank=True, null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now_add=True)
    created_ts =  models.DateTimeField(auto_now_add=True)   
    is_staff = models.BooleanField(default=False) 


    objects = CustomUserManager()

    class Meta:
        verbose_name_plural = 'User'

    def __str__(self):
        return self.email



class Account(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=False, null=False)
    account_address = models.CharField(max_length=255, blank=False, null=False)
    balance = models.CharField(max_length=15, blank=True, null=True)
    created_ts = models.DateTimeField(auto_now_add=False, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Account'

    def __str__(self):
        return self.user.email



class UserAuthentication(models.Model):
    user = models.OneToOneField(to=User,on_delete=models.CASCADE, blank=False, null=False)
    national_number = models.CharField(max_length=10, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    national_card_image = models.FileField(upload_to='cards')
    user_with_card_image = models.FileField(upload_to='users')
    is_authorized = models.BooleanField(default=False)
    created_ts =  models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'Authentication'

    def __str__(self):
        return self.user.email

    def save(self, *args, **kwargs):
        self.user.is_authorized = self.is_authorized
        self.user.save()
        super(UserAuthentication, self).save(*args, **kwargs)


class Ticket(models.Model):
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=False, null=False)
    title = models.CharField(max_length=100, blank=False, null=False)
    email = models.EmailField(blank=False, null=False)
    image = models.FileField(upload_to='tickets', blank=True, null=True)
    description = models.TextField(blank=False, null=False)
    is_solved = models.BooleanField(default=False, blank=True)
    created_ts =  models.DateTimeField(auto_now_add=True)


    class Meta:
        verbose_name_plural = 'Ticket'

    def __str__(self):
        return self.title

from enumchoicefield import ChoiceEnum, EnumChoiceField

class RequestStatus(ChoiceEnum):
    pending = "در حال بررسی"
    approved = "تایید شده"
    rejected = "رد شده"
    
    def __repr__(self) -> str:
        return self.value


class UserRequest(models.Model):

    
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=False, null=False)
    account = models.ForeignKey(to=Account, on_delete=models.CASCADE, null=False, blank=False)
    title = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=True, null=True)
    status = EnumChoiceField(enum_class=RequestStatus, default=RequestStatus.pending)
    created_ts = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'UserRequest'

    def __str__(self):
        return self.creator.email


class WithdrawalRequest(models.Model):
    creator = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=False, null=False)
    amount = models.CharField(max_length=15, blank=False, null=False)
    created_ts = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    status = models.CharField(max_length=255, blank=False, null=False)
    
    
    class Meta:
        verbose_name_plural = 'WithdrawalRequest'

    def __str__(self):
        return self.creator.email
    

class Challenges(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    price = models.IntegerField(blank=False, null=False)
    first_step_max_days = models.CharField(max_length=255, blank=True, null=True)
    second_step_max_days = models.CharField(max_length=255, blank=True, null=True)
    third_step_max_days = models.CharField(max_length=255, blank=True, null=True)
    first_step_min_days = models.CharField(max_length=255, blank=True, null=True)
    second_step_min_days = models.CharField(max_length=255, blank=True, null=True)
    third_step_min_days = models.CharField(max_length=255, blank=True, null=True)    
    first_step_max_daily_damage = models.CharField(max_length=255, blank=True, null=True)
    second_step_max_daily_damage = models.CharField(max_length=255, blank=True, null=True)
    third_step_max_daily_damage = models.CharField(max_length=255, blank=True, null=True)
    first_step_max_damage = models.CharField(max_length=255, blank=True, null=True)
    second_step_max_damage = models.CharField(max_length=255, blank=True, null=True)
    third_step_max_damage = models.CharField(max_length=255, blank=True, null=True)
    first_step_sufficient_profit = models.CharField(max_length=255, blank=True, null=True)
    second_step_sufficient_profit = models.CharField(max_length=255, blank=True, null=True)
    third_step_sufficient_profit = models.CharField(max_length=255, blank=True, null=True)
    first_step_platform = models.CharField(max_length=255, blank=True, null=True)
    second_step_platform = models.CharField(max_length=255, blank=True, null=True)
    third_step_platform = models.CharField(max_length=255, blank=True, null=True)
    first_step_one_time_purchase_price = models.CharField(max_length=255, blank=True, null=True)
    second_step_one_time_purchase_price = models.CharField(max_length=255, blank=True, null=True)
    third_step_one_time_purchase_price = models.CharField(max_length=255, blank=True, null=True)
    created_ts = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = 'Challenge'

    def __str__(self):
        return self.name


class UserChallenges(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, blank=False, null=False)
    challenges = models.ForeignKey(to=Challenges, on_delete=models.CASCADE, blank=False, null=False)
    status = models.CharField(max_length=255, blank=True, null=True)
    created_ts = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    step = models.CharField(max_length=255, blank=True, null=True)
    purchase_id = models.CharField(max_length=255, blank=True, null=True)
    purchase_ts = models.DateTimeField(auto_now_add=False, blank=False, null=False)
    purchase_amount = models.CharField(max_length=12, blank=True, null=True)
    purchase_status = models.CharField(max_length=255, blank=True, null=True)
    invoice_url = models.CharField(max_length=255, blank=True, null=True)
    price_currency = models.CharField(max_length=255, blank=True, null=True)
    pay_currency = models.CharField(max_length=255, blank=True, null=True)
    order_id = models.CharField(max_length=255, blank=True, null=True)
    pay_address = models.CharField(max_length=255, blank=True, null=True)
    actually_paid = models.CharField(max_length=255, blank=True, null=True)
    invoice_id = models.CharField(max_length=255, blank=True, null=True)
    pay_amount = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'UserChallenges'

    def __str__(self):
        return self.user.email